const axios = require("axios");

const doPostNews = async (title, slug, content) => {
  try {
    const res = await axios.post("http://localhost:1337/api/articles", {
      data: {
        title,
        slug,
        content,
      },
    });
    console.log(res.status, "-", res.statusText);
  } catch (error) {
    console.log(error);
  }
};

const rawJson = require("fs").readFileSync(
  "./assets/data-without-revamp.json",
  "utf-8"
);

const data = JSON.parse(rawJson);
const newPayload = data.rss.channel.item.map((e) => ({
  title: e.title.__cdata,
  slug: e.post_name.__cdata,
  content: e.encoded[0].__cdata,
}));
console.log("Total ", newPayload.length, " records");

// main action
newPayload.map(async (e) => {
  try {
    await doPostNews(e.title, e.slug, e.content);
  } catch (error) {
    console.log("ERROR");
  }
});
